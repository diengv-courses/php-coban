<?php
$sql = "SELECT posts.id, posts.title, posts.author, posts.created_at, categories.name as category_name 
FROM posts 
    INNER JOIN categories ON posts.category_id = categories.id 
ORDER BY posts.created_at DESC";
$result = $conn->query($sql);

$conn->close();