<?php
$isEdit = false;
$post = [
    'id' => '',
    'title' => '',
    'content' => '',
    'author' => '',
    'category_id' => ''
];

// Lấy danh sách categories
$sql = "SELECT * FROM categories";
$categories = $conn->query($sql);

// Nếu là trang chỉnh sửa bài post
if (isset($_GET["id"])) {
    $isEdit = true;
    $post_id = $_GET["id"];

    // Lấy thông tin bài post
    $sql = "SELECT * FROM posts WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $post_id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $post = $result->fetch_assoc();
    } else {
        echo "Post not found.";
        exit();
    }

    $stmt->close();
}

// Xử lý khi người dùng submit biểu mẫu
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST["title"];
    $content = $_POST["content"];
    $author = $_POST["author"];
    $category_id = $_POST["category_id"];

    if ($isEdit) {
        $sql = "UPDATE posts SET title = ?, content = ?, author = ?, category_id = ? WHERE id = ?";
        $stmt = $conn->prepare($sql);
        /**
         * Chuỗi "sssii" cho biết rằng chúng ta sẽ truyền vào 3 tham số kiểu chuỗi đầu tiên, và sau đó là 2 tham số kiểu số nguyên.
         * Các tham số sau chuỗi định dạng sẽ được ràng buộc theo đúng thứ tự và kiểu dữ liệu đã chỉ định.
         */
        $stmt->bind_param("sssii", $title, $content, $author, $category_id, $post_id);
    } else {
        $sql = "INSERT INTO posts (title, content, author, category_id) VALUES (?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssi", $title, $content, $author, $category_id);
    }

    if ($stmt->execute()) {
        header("Location: /post/index.php");
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $stmt->close();
    $conn->close();
}