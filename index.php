<?php
require_once 'db/config.php';
require_once 'db/post-index.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+Knujslafg0p/grPfFw7f37K_hsJx7q79ydA3jcYj+AJm9Pkb" crossorigin="anonymous">
    <link href="/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Blog</h1>

    <h2>10 Latest Posts</h2>
    <div class="row">
        <?php
        if ($result_top20->num_rows > 0) {
            $count = 0;
            while ($row = $result_top20->fetch_assoc() && $count < 10) {
                $count++;
                ?>
                <div class="col-md-6">
                    <h3><?php echo $row["title"]; ?></h3>
                    <p>by <?php echo $row["author"]; ?> on <?php echo $row["created_at"]; ?></p>
                    <p><?php echo $row["content"]; ?></p>
                </div>
                <?php
            }
        } else {
            echo "No posts found.";
        }
        ?>
    </div>

    <h2>All Posts</h2>
    <div class="row">
        <?php
        if ($result->num_rows > 0) {
            $count = 0;
            while ($row = $result->fetch_assoc()) {
                if ($count % 5 === 0) {
                    echo '</div><div class="row mt-4">';
                }
                $count++;
                ?>
                <div class="col-md-2-4">
                    <h3><?php echo $row["title"]; ?></h3>
                    <p>by <?php echo $row["author"]; ?> on <?php echo $row["created_at"]; ?></p>
                    <p><?php echo $row["content"]; ?></p>
                </div>
                <?php
            }
        } else {
            echo "No posts found.";
        }
        ?>
    </div>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <?php for ($i = 1; $i <= $totalPages; $i++) { ?>
                <li class="page-item <?php if ($page == $i) echo 'active'; ?>">
                    <a class="page-link" href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                </li>
            <?php } ?>
        </ul>
    </nav>
</div>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz4fnFO9gybB5cyb1b84CXr0pWt+80+C3r7K9z2/a1Coe58tIj66y2Qr8h"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"
        integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/"
        crossorigin="anonymous"></script>
</body>
</html>
