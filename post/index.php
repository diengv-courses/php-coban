<?php
require_once '../db/config.php';
require_once '../db/admin/fetch_posts.php';

?>

<!DOCTYPE html>
<html lang="en">
<?php
$page_title = 'Post manager';
include '../layout/admin/header.php'; ?>
<body>
<div class="container">
    <h1>Post Management</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
            <th scope="col">Category</th>
            <th scope="col">Created At</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <th scope="row"><?php echo $row["id"]; ?></th>
                    <td><?php echo $row["title"]; ?></td>
                    <td><?php echo $row["author"]; ?></td>
                    <td><?php echo $row["category_name"]; ?></td>
                    <td><?php echo $row["created_at"]; ?></td>
                </tr>
                <?php
            }
        } else {
            echo "<tr><td colspan='5'>No posts found.</td></tr>";
        }
        ?>
        </tbody>
    </table>
</div>

<?php include '../layout/admin/footer.php'; ?>
</body>
</html>
