<?php
require_once '../db/config.php';
require_once '../db/admin/add_edit_post.php';
?>

<!DOCTYPE html>
<html lang="en">
<?php
$page_title = $isEdit ? 'Edit' : 'Create New';
include '../layout/admin/header.php'; ?>
<body>
<div class="container">
    <h1><?php echo $isEdit ? 'Edit' : 'Create New'; ?> Post</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?><?php echo $isEdit ? "?id=" . $post_id : ""; ?>"
          method="post">
        <div class="mb-3">
            <label for="title" class="form-label">Title:</label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo $post['title']; ?>"
                   required>
        </div>
        <div class="mb-3">
            <label for="content" class="form-label">Content:</label>
            <textarea class="form-control" id="content" name="content" rows="5"
                      required><?php echo $post['content']; ?></textarea>
        </div>
        <div class="mb-3">
            <label for="author" class="form-label">Author:</label>
            <input type="text" class="form-control" id="author" name="author" value="<?php echo $post['author']; ?>"
                   required>
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Category:</label>
            <select class="form-select" id="category_id" name="category_id" required>
                <option value="">Choose a category</option>
                <?php while ($category = $categories->fetch_assoc()) { ?>
                    <option value="<?php echo $category['id']; ?>" <?php echo $post['category_id'] == $category['id'] ? 'selected' : ''; ?>>
                        <?php echo $category['name']; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary"><?php echo $isEdit ? 'Save Changes' : 'Create Post'; ?></button>
    </form>
</div>

<?php include '../layout/admin/footer.php'; ?>
</body>
</html>

